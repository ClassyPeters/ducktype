# DuckType

The soft and fuzzy typed language unless you get a goose

One of the many things involved in building and creating programming languages is typing. As in what type of thing is a thing. Is it an integer, a boolean, string, floating point number, or something else entirely?

Obviously the most elegant and simple for a programmer to use is 'Duck-Typing'. As in 'If it looks like a duck, quacks like a duck, and walks like a duck, we'll treat it like a duck!' (substituting your favorite type for duck) 

In DuckType, there are three types: Rows, Ducks, and Geese, and untyped birds will become either a Duck or a Goose depending on where they are in a row.

Rows are a first in first out execution functions. In a row, operators are put between the birds.

The first untyped bird in a row is typed as a Duck, and is called the Mother Duck (or Mother Type). From there all birds after that have the same attributes as the Mother are also Ducks (typlings).

Geese are the other type, and are any birds in a row that aren't a Duck. When a goose is found in a row, it wil hiss, chase the other ducks, and throw an exception and end the row (what truly foul waterfowl). What's good for the goose is not good for the mallard. 

<h2>Syntax</h2>
Since the typing is already fuzzy and cuddly, it feels right to have the syntax be strictly defined with some fuzziness (much like feathers). Syntax is bracket based with spaces inside of brackets.

Let's define a bird.
<p>
Bird{attribute_name:attribute_value}

<p>
Rows are defined inside of brackets, with operators instead of commas.
<p>
Let's define a row:
Row{Mother_Duck + typling * typling - typling / typling}


